package com.example.demo.controller;

import com.example.demo.entities.Empleado;
import com.example.demo.services.IEmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manage")
public class EmpleadoController {

    @Autowired
    private IEmpleadoService empleadoService;

    @PostMapping(value = "empleado", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado empleado){
        empleadoService.insert(empleado);
        return new ResponseEntity<>(empleadoService.getById(empleado.getId()), HttpStatus.OK);
    }

    @GetMapping(value = "empleado")
    public ResponseEntity<List<Empleado>> getAllEmpleados(){
        List<Empleado> result = empleadoService.getAll();
        if(result == null || result.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("empleado/{empleadoId}")
    public ResponseEntity<?> getEmpleadoById(@PathVariable("empleadoId") int id){
        Empleado result = empleadoService.getById(id);
        if(result == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @PutMapping(value = "empleado", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateEmpleado(@RequestBody Empleado empleado){
        if(empleadoService.update(empleado)){
            return new ResponseEntity<>(empleadoService.getById(empleado.getId()), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
    }

    @DeleteMapping("empleado/{empleadoId}")
    public ResponseEntity<?>deleteEmpleado(@PathVariable("empleadoId") Integer id){
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        empleadoService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);

    }
}
