package com.example.demo.controller;

import com.example.demo.entities.Order;
import com.example.demo.services.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/manage")
public class OrderController {

    @Autowired
    private IOrderService orderService;

    @PostMapping(value="order", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> addOrder(@RequestBody Order order) {
        orderService.addOrder(order);
        return new ResponseEntity<Object> ( HttpStatus.OK);
    }

    @GetMapping("order")
    public ResponseEntity<?> getAllOrders() {
        final List<Order> orderList = orderService.getAllOrders();
        if (orderList.isEmpty()) {
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Order>>(orderList, HttpStatus.OK);
    }

    @GetMapping("order/{employeeID}")
    public ResponseEntity<?> getOrdersById(@PathVariable("employeeID") int employeeID) {
        // Verify Contact ID exists
        final List<Order> orderList = orderService.getOrdersById(employeeID);
        if (orderList.isEmpty()) {
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<Order>>(orderList, HttpStatus.OK);
    }

    @PutMapping("order")
    public ResponseEntity<?> updateOrder (@RequestBody Order order) {
        List<Order> currentOrder = orderService.getOrdersById(order.getId());

        if (currentOrder == null) {
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }
        orderService.updateOrder(order);
        return new ResponseEntity<Object>( HttpStatus.OK);
    }

    @DeleteMapping("order/{orderId}")
    public ResponseEntity<?> deleteOrder (@PathVariable("orderId") int orderId) {
        List<Order> currentOrder = orderService.getOrdersById(orderId);
        if (currentOrder == null) {
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }
        orderService.deleteOrder(orderId);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

}
