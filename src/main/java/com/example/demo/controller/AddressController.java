package com.example.demo.controller;

import com.example.demo.entities.Address;
import com.example.demo.services.IAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manage")
public class AddressController {

    @Autowired
    private IAddressService addressService;


    @PostMapping(value = "address", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> addAddress(@RequestBody Address address){
        addressService.insert(address);
        return new ResponseEntity<>(address.getStreet(), HttpStatus.OK);
    }

    @GetMapping(value = "address")
    public ResponseEntity<List<Address>> getAllAddress(){
        List<Address> result = addressService.getAll();
        if(result == null || result.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("address/{addressId}")
    public ResponseEntity<?> getContactById(@PathVariable("addressId") int id){
        Address result = addressService.getById(id);
        if(result == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping(value = "address", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateAddress(@RequestBody Address address){
        if(addressService.update(address)){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }

    }

    @DeleteMapping("address/{addressId}")
    public ResponseEntity<?>deleteAddress(@PathVariable("addressId") Integer id){
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        addressService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);

    }

}
