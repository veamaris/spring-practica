package com.example.demo.controller;

import com.example.demo.entities.Contact;
import com.example.demo.services.IContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manage")
public class ContactController {

    @Autowired
    @Qualifier("contactService")
    private IContactService contactService;

    @PostMapping(value = "contacts", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> addContact(@RequestBody Contact contact){
        contactService.addContact(contact);
        return new ResponseEntity<>("Contacto añadido: "+contact.getName(), HttpStatus.OK);
    }


    @GetMapping(value = "contacts")
    public ResponseEntity<List<Contact>> getAllContacts(){
        List<Contact> result = contactService.getAllContacts();
        if(result == null || result.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("contacts/{contactId}")
    public ResponseEntity<?> getContactById(@PathVariable("contactId") int id){
        Contact result = contactService.getByContactId(id);
        if(result == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }



    @PutMapping(value = "contacts/{contactId}/{contactName}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateContact(@PathVariable("contactId") int id, @PathVariable("contactName") String name){
        if(contactService.updateContactName(name, id)){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }

    }

    @DeleteMapping("contacts/{contactId}")
    public ResponseEntity<?>deleteContact(@PathVariable("contactId") Integer id){
            if(id == null){
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        if(contactService.deleteContact(id)){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
    }
}
