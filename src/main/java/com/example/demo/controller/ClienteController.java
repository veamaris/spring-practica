package com.example.demo.controller;

import com.example.demo.entities.Client;
import com.example.demo.services.IClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manage")
public class ClienteController {
    @Autowired
    private IClientService clientService;

    @PostMapping(value = "client", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> addClient(@RequestBody Client client){
        clientService.addClient(client);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "client")
    public ResponseEntity<List<Client>> getAllClients(){
        List<Client> result = clientService.getAllClients();
        if(result == null || result.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("client/{clientId}")
    public ResponseEntity<?> getContactById(@PathVariable("clientId") int id){
        Client result = clientService.getByClientId(id);
        if(result == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping(value = "client", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateAddress(@RequestBody Client client){
        if(clientService.updateClient(client)){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }

    }

    @DeleteMapping("client/{clientId}")
    public ResponseEntity<?>deleteClient(@PathVariable("clientId") Integer id){
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        clientService.deleteClient(id);
        return new ResponseEntity<>(HttpStatus.OK);

    }

}
