package com.example.demo.controller;

import com.example.demo.entities.Proovedor;
import com.example.demo.services.ProovedorServidor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manage")
public class ProovedorController {
    
    @Autowired
    private ProovedorServidor proovedorServidor;


    @PostMapping(value = "proovedor", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> addAddress(@RequestBody Proovedor proovedor){
        proovedorServidor.insert(proovedor);
        return new ResponseEntity<>(proovedor, HttpStatus.OK);
    }

    @GetMapping(value = "proovedor")
    public ResponseEntity<List<Proovedor>> getAllAddress(){
        List<Proovedor> result = proovedorServidor.getAll();
        if(result == null || result.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("proovedor/{proovedorId}")
    public ResponseEntity<?> getContactById(@PathVariable("proovedorId") int id){
        Proovedor result = proovedorServidor.getById(id);
        if(result == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping(value = "proovedor", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateAddress(@RequestBody Proovedor proovedor){
        if(proovedorServidor.update(proovedor)){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }

    }

    @DeleteMapping("proovedor/{proovedorId}")
    public ResponseEntity<?>deleteAddress(@PathVariable("proovedorId") Integer id){
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        proovedorServidor.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);

    }
}
