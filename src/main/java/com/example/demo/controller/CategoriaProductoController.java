package com.example.demo.controller;

import com.example.demo.entities.CategoriaProducto;
import com.example.demo.services.ICategoriaProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manage")
public class CategoriaProductoController {

    @Autowired
    private ICategoriaProducto categoriaProductoService;

    @GetMapping(value = "categoria")
    public ResponseEntity<List<CategoriaProducto>> getAll(){
        List<CategoriaProducto> result = categoriaProductoService.getAll();
        if(result == null || result.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @GetMapping("categoria/{categoryId}")
    public ResponseEntity<?> getCategoryById(@PathVariable("categoryId") int id){
        CategoriaProducto result = categoriaProductoService.getById(id);
        if(result == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(value = "categoria", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> addAddress(@RequestBody CategoriaProducto categoriaProducto){
        categoriaProductoService.insert(categoriaProducto);
        return new ResponseEntity<>(categoriaProducto, HttpStatus.OK);
    }

    @PutMapping(value = "categoria", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateAddress(@RequestBody CategoriaProducto categoriaProducto){
        if(categoriaProductoService.update(categoriaProducto)){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }

    }

    @DeleteMapping("categoria/{categoriaId}")
    public ResponseEntity<?>deleteAddress(@PathVariable("categoriaId") Integer id){
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        categoriaProductoService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);

    }





}
