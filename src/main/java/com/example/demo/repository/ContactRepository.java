package com.example.demo.repository;

import com.example.demo.entities.Address;
import com.example.demo.entities.Contact;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ContactRepository {
    @Insert("INSERT INTO CONTACT (CONTACT_NAME, CONTACT_EMAIL, CONTACT_PHONE) VALUES (#{name},#{email}, #{phone})")
    //Para autoincrementar el id hay que usar useGeneratedKeys y especificar la columna del id en la tabla
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(final Contact contact);

    @Select("SELECT * FROM CONTACT")
    @Results(value = {
            @Result(property = "id", column = "CONTACT_ID"),
            @Result(property = "name", column = "CONTACT_NAME"),
            @Result(property = "phone", column = "CONTACT_PHONE"),
            @Result(property = "email", column = "CONTACT_EMAIL"),
            @Result(property = "addressList", javaType = List.class, column = "CONTACT_ID", many = @Many(select = "getAddresses"))
    })
    List<Contact> getAllContacts();

    @Select("SELECT * FROM CONTACT WHERE CONTACT_ID = #{id}")
    @Results(value = {
            @Result(property = "id", column = "CONTACT_ID"),
            @Result(property = "name", column = "CONTACT_NAME"),
            @Result(property = "phone", column = "CONTACT_PHONE"),
            @Result(property = "email", column = "CONTACT_EMAIL")
    })
    Contact getContactById(int id);

    @Select("SELECT * FROM ADDRESS WHERE CONTACTO = #{id}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "contact", column = "CONTACTO"),
    })
    Address getAddresses(int id);


    @Update("UPDATE CONTACT SET CONTACT_NAME = #{name},CONTACT_EMAIL = #{email}, CONTACT_PHONE = #{phone} WHERE CONTACT_ID = #{id}")
    void updateContact(final Contact contact);

    @Update("UPDATE CONTACT SET CONTACT_NAME = #{name} WHERE CONTACT_ID = #{id}")
    void updateContactName(@Param("name") String name, @Param("id") int id);

    @Delete("DELETE FROM CONTACT WHERE CONTACT_ID = #{id}")
    void deleteContact(int id);
}
