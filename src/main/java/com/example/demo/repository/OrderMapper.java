package com.example.demo.repository;

import com.example.demo.entities.*;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface OrderMapper {


    @Insert("INSERT INTO `ORDER_COMPRA` (`ID`, `CLIENTE`, `SHIPPING`, `FECH_COMPRA`, `FECH_ENVIO`, `FECH_ENTR`, `DIRECCION`, `CIUDAD`, `REGION`, `CP`, `PAIS`) VALUES (#{employeeId}, #{clientId}, #{shippingId}, #{dateOrdered}," +
            " #{dateSent}, #{deliverDate}, #{address}, #{city}, #{region}, #{zipcode}, #{country})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(final Order order);

    @Select("SELECT * FROM `ORDER_COMPRA`")
    @Results(value = {
            @Result(property = "employeeId", column = "ID"),
            @Result(property = "clientId", column = "CLIENTE"),
            @Result(property = "shippingId", column = "SHIPPING"),
            @Result(property = "dateOrdered", column = "FECH_COMPRA"),
            @Result(property = "dateSent", column = "FECH_ENVIO"),
            @Result(property = "deliverDate", column = "FECH_ENTR"),
            @Result(property = "address", column = "DIRECCION"),
            @Result(property = "city", column = "CIUDAD"),
            @Result(property = "region", column = "REGION"),
            @Result(property = "zipcode", column = "CP"),
            @Result(property = "country", column = "PAIS"),
            @Result(property = "employee", javaType = Contact.class, column = "ID",
                    one = @One(select = "getEmployee")),
            @Result(property = "client", javaType = Contact.class, column = "CLIENTE",
                    one = @One(select = "getClient")),
            @Result(property = "shipping", javaType = Contact.class, column = "SHIPPING",
                    one = @One(select = "getShipping")),
    })
    List<Order> selectAll();

    @Select("SELECT * FROM `EMPLEADOS` WHERE `ID`=#{id}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NOMBRE"),
            @Result(property = "apellido", column = "APELLIDO"),
            @Result(property = "titulo", column = "TITULO"),
            @Result(property = "fech_nac", column = "FECH_NAC"),
            @Result(property = "fech_contr", column = "FECH_CONTR"),
            @Result(property = "direccion", column = "DIRECCION"),
            @Result(property = "ciudad", column = "CIUDAD"),
            @Result(property = "region", column = "REGION"),
            @Result(property = "cp", column = "CP"),
            @Result(property = "pais", column = "PAIS"),
            @Result(property = "tlf", column = "TLF"),
            @Result(property = "foto", column = "FOTO"),
            @Result(property = "report", column = "REPORT")
    })
    Empleado getEmployee(int id);

    @Select("SELECT * FROM `CLIENTE` WHERE `id`=#{id}")
    Client getClient(int id);

    @Select("SELECT * FROM `SHIPPING` WHERE `id`=#{id}")
    Shipping getShipping(int id);

    @Select("SELECT * FROM ORDER_COMPRA WHERE `id` = #{id}")
    @Results(value = {
            @Result(property = "employeeId", column = "ID"),
            @Result(property = "clientId", column = "CLIENTE"),
            @Result(property = "shippingId", column = "SHIPPING"),
            @Result(property = "dateOrdered", column = "FECH_COMPRA"),
            @Result(property = "dateSent", column = "FECH_ENVIO"),
            @Result(property = "deliverDate", column = "FECH_ENTR"),
            @Result(property = "address", column = "DIRECCION"),
            @Result(property = "city", column = "CIUDAD"),
            @Result(property = "region", column = "REGION"),
            @Result(property = "zipcode", column = "CP"),
            @Result(property = "country", column = "PAIS"),
            @Result(property = "employee", javaType = Contact.class, column = "ID",
                    one = @One(select = "getEmployee")),
            @Result(property = "client", javaType = Contact.class, column = "CLIENTE",
                    one = @One(select = "getClient")),
            @Result(property = "shipping", javaType = Contact.class, column = "SHIPPING",
                    one = @One(select = "getShipping")),
    })
    List<Order> selectById(int id);

    /*@Select("SELECT CONTACT_ID FROM CONTACT WHERE CONTACT_ID=#{id}")
    Contact getContacts(int id);*/

    @Update("UPDATE ORDER_COMPRA SET `ID` = #{employeeId}, `CLIENTE` = #{clientId}, `SHIPPING` = #{shippingId}, `FECH_COMPRA` = #{dateOrdered}, `FECH_ENVIO`= #{dateSent}, `FECH_ENTR` = #{deliverDate}," +
            " `DIRECCION` = #{address}, `CIUDAD` = #{city}, `REGION`= #{region}, `CP` = #{zipcode}, `PAIS`= #{country} WHERE ID = #{id}")
    void update(Order order);

    @Delete("DELETE FROM ORDER_COMPRA WHERE ID = #{id}")
    void delete(int id);

}
