package com.example.demo.repository;

import com.example.demo.entities.Client;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ClientMapper {

    @Insert("INSERT INTO CLIENTE (NOMBRE, DIRECCION, CIUDAD, REGION, CP, PAIS, TLF, FAX, TITULO) VALUES (#{name},#{direccion}, #{ciudad}, #{region}, #{cp}, #{pais}, #{tlf}, #{fax}, #{titulo})")
    //Para autoincrementar el id hay que usar useGeneratedKeys y especificar la columna del id en la tabla
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(final Client client);

    @Select("SELECT * FROM CLIENTE")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NOMBRE"),
            @Result(property = "direccion", column = "DIRECCION"),
            @Result(property = "ciudad", column = "CIUDAD"),
            @Result(property = "region", column = "REGION"),
            @Result(property = "cp", column = "CP"),
            @Result(property = "pais", column = "PAIS"),
            @Result(property = "tlf", column = "TLF"),
            @Result(property = "fax", column = "FAX"),
            @Result(property = "titulo", column = "TITULO")
    })
    List<Client> getAllClients();

    @Select("SELECT * FROM CLIENTE WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NOMBRE"),
            @Result(property = "direccion", column = "DIRECCION"),
            @Result(property = "ciudad", column = "CIUDAD"),
            @Result(property = "region", column = "REGION"),
            @Result(property = "cp", column = "CP"),
            @Result(property = "pais", column = "PAIS"),
            @Result(property = "tlf", column = "TLF"),
            @Result(property = "fax", column = "FAX"),
            @Result(property = "titulo", column = "TITULO")
    })
    Client getClientById(int id);


    @Update("UPDATE CLIENTE SET NOMBRE = #{name},DIRECCION = #{direccion}, CIUDAD = #{ciudad}, REGION = #{region}, CP = #{cp}, PAIS = #{pais}, TLF = #{tlf}, FAX = #{fax}, TITULO = #{titulo} WHERE ID = #{id}")
    void updateClient(final Client client);

    @Update("UPDATE CLIENTE SET NOMBRE = #{name} WHERE ID = #{id}")
    void updateClientName( String name, int id);

    @Delete("DELETE FROM CLIENTE WHERE ID = #{id}")
    void deleteClient(int id);

}
