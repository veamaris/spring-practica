package com.example.demo.repository;

import com.example.demo.entities.Empleado;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface EmpleadoMapper {

    @Insert("INSERT INTO EMPLEADOS (NOMBRE, APELLIDO, TITULO, FECH_NAC, FECH_CONTR, DIRECCION, CIUDAD, REGION, CP, PAIS, TLF, FOTO, REPORT) VALUES (#{name}, #{apellido}, #{titulo}, #{fech_nac}, #{fech_contr}, #{direccion}, #{ciudad}, #{region}, #{cp}, #{pais}, #{tlf}, #{foto}, #{report})")
    //Para autoincrementar el id hay que usar useGeneratedKeys y especificar la columna del id en la tabla
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insertEmpleados(final Empleado entity);

    @Select("SELECT * FROM EMPLEADOS")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NOMBRE"),
            @Result(property = "apellido", column = "APELLIDO"),
            @Result(property = "titulo", column = "TITULO"),
            @Result(property = "fech_nac", column = "FECH_NAC"),
            @Result(property = "fech_contr", column = "FECH_CONTR"),
            @Result(property = "direccion", column = "DIRECCION"),
            @Result(property = "ciudad", column = "CIUDAD"),
            @Result(property = "region", column = "REGION"),
            @Result(property = "cp", column = "CP"),
            @Result(property = "pais", column = "PAIS"),
            @Result(property = "tlf", column = "TLF"),
            @Result(property = "foto", column = "FOTO"),
            @Result(property = "report", column = "REPORT")
    })
    List<Empleado> getAllEmpleados();

    @Select("SELECT * FROM EMPLEADOS WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NOMBRE"),
            @Result(property = "apellido", column = "APELLIDO"),
            @Result(property = "titulo", column = "TITULO"),
            @Result(property = "fech_nac", column = "FECH_NAC"),
            @Result(property = "fech_contr", column = "FECH_CONTR"),
            @Result(property = "direccion", column = "DIRECCION"),
            @Result(property = "ciudad", column = "CIUDAD"),
            @Result(property = "region", column = "REGION"),
            @Result(property = "cp", column = "CP"),
            @Result(property = "pais", column = "PAIS"),
            @Result(property = "tlf", column = "TLF"),
            @Result(property = "foto", column = "FOTO"),
            @Result(property = "report", column = "REPORT")
    })
    Empleado getEmpleadosById(int id);


    @Update("UPDATE EMPLEADOS SET NOMBRE = #{name},APELLIDO = #{apellido}, TITULO = #{titulo},FECH_NAC = #{fech_nac},FECH_CONTR = #{fech_contr}, DIRECCION = #{direccion}, REGION = #{region}, CIUDAD = #{ciudad}, CP = #{cp}, PAIS = #{pais}, TLF = #{tlf}, FOTO = #{foto}, REPORT = #{report} WHERE ID = #{id}")
    void updateEmpleados(final Empleado entity);

    @Update("UPDATE EMPLEADOS SET NOMBRE = #{name} WHERE ID = #{id}")
    void updateEmpleadosName( String name, int id);

    @Delete("DELETE FROM EMPLEADOS WHERE ID = #{id}")
    void deleteEmpleados(int id);
}
