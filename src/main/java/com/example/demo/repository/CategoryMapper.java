package com.example.demo.repository;



import com.example.demo.entities.CategoriaProducto;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryMapper {


    @Insert("INSERT INTO CATEGORIA_PRODUCTO (NOMBRE, DESCRIPCION, IMG) VALUES (#{name}, #{descripcion}, #{foto})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(final CategoriaProducto categoriaProducto);


    @Select("SELECT * FROM CATEGORIA_PRODUCTO WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NOMBRE"),
            @Result(property = "descripcion", column = "DESCRIPCION"),
            @Result(property = "foto", column = "IMG"),

    })
    CategoriaProducto getCategoriaById(int id);

    @Select("SELECT * FROM CATEGORIA_PRODUCTO")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NOMBRE"),
            @Result(property = "descripcion", column = "DESCRIPCION"),
            @Result(property = "foto", column = "IMG"),

    })
    List<CategoriaProducto> getAllCategorias();


    @Delete("DELETE FROM CATEGORIA_PRODUCTO WHERE ID = #{id}")
    void deleteCategoria(int id);

    @Update("UPDATE CATEGORIA_PRODUCTO SET NOMBRE = #{name}, DESCRIPCION = #{descripcion}, IMG = #{foto} WHERE ID = #{id}")
    void updateCategoria(final CategoriaProducto categoriaProducto);



}
