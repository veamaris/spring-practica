package com.example.demo.repository;

import com.example.demo.entities.Proovedor;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ProovedorMapper {

    @Insert("INSERT INTO PROOVEDOR (NOMBRE, TITULO, CONTACTO, DIRECCION, PAIS, CP, CIUDAD, REGION, TLF, FAX, PAG_WEB) VALUES (#{name}, #{titulo},#{contacto},#{direccion}, #{pais}, #{cp}, #{ciudad}, #{region}, #{tlf}, #{fax}, #{pagWeb})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insertProovedor(Proovedor proovedor);


    @Select("SELECT * FROM PROOVEDOR WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NOMBRE"),
            @Result(property = "titulo", column = "TITULO"),
            @Result(property = "contacto", column = "CONTACTO"),
            @Result(property = "direccion", column = "DIRECCION"),
            @Result(property = "pais", column = "PAIS"),
            @Result(property = "ciudad", column = "CIUDAD"),
            @Result(property = "region", column = "REGION"),
            @Result(property = "cp", column = "CP"),
            @Result(property = "pais", column = "PAIS"),
            @Result(property = "tlf", column = "TLF"),
            @Result(property = "fax", column = "FAX"),
            @Result(property = "pagWeb", column = "PAG_WEB")
    })
    Proovedor getProovedoresId(int id);

    @Select("SELECT * FROM PROOVEDOR")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NOMBRE"),
            @Result(property = "titulo", column = "TITULO"),
            @Result(property = "contacto", column = "CONTACTO"),
            @Result(property = "pais", column = "PAIS"),
            @Result(property = "direccion", column = "DIRECCION"),
            @Result(property = "ciudad", column = "CIUDAD"),
            @Result(property = "region", column = "REGION"),
            @Result(property = "cp", column = "CP"),
            @Result(property = "pais", column = "PAIS"),
            @Result(property = "tlf", column = "TLF"),
            @Result(property = "fax", column = "FAX"),
            @Result(property = "pagWeb", column = "PAG_WEB")
    })
    List<Proovedor> getAllProovedores();

    @Delete("DELETE FROM PROOVEDOR WHERE ID = #{id}")
    void deleteProovedor(int id);

    @Update("UPDATE PROOVEDOR SET NOMBRE = #{name}, TITULO = #{titulo}, CONTACTO = #{contacto}, DIRECCION = #{direccion}, CIUDAD = #{ciudad}, REGION = #{region}, PAIS = #{pais}, TLF = #{tlf}, FAX = #{fax} , PAG_WEB = #{pagWeb} WHERE ID = #{id}")
    void updateProovedor(final Proovedor proovedor);


}
