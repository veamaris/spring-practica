package com.example.demo.repository;

import com.example.demo.entities.Address;
import com.example.demo.entities.Contact;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AddressMapper {


    @Insert("INSERT INTO ADDRESS (CONTACTO, STREET) VALUES (#{contact}, #{street})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insertAddress(Address address);

    @Select("SELECT * FROM ADDRESS WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "contact", column = "CONTACTO"),
            @Result(property = "street", column = "STREET"),
            @Result(property = "contactClass", javaType = Contact.class,column = "CONTACTO", one = @One(select = "getContactById"))
    })
    Address getAddressById(int id);

    @Select("SELECT * FROM ADDRESS")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "contact", column = "CONTACTO"),
            @Result(property = "street", column = "STREET"),
            @Result(property = "contactClass", javaType = Contact.class,column = "CONTACTO", one = @One(select = "getContactById"))
    })
    List<Address> getAllAddresses();

    @Delete("DELETE FROM ADDRESS WHERE ID = #{id}")
    void deleteAddress(int id);

    @Update("UPDATE ADDRESS SET STREET = #{street}, CONTACTO = #{contact} WHERE ID = #{id}")
    void updateAddress(final Address address);

    @Select("SELECT * FROM CONTACT WHERE CONTACT_ID = #{id}")
    @Results(value = {
            @Result(property = "id", column = "CONTACT_ID"),
            @Result(property = "name", column = "CONTACT_NAME"),
            @Result(property = "phone", column = "CONTACT_PHONE"),
            @Result(property = "email", column = "CONTACT_EMAIL")
    })
    Contact getContactById(int id);

}




