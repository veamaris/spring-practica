package com.example.demo.services;

import com.example.demo.entities.Client;
import com.example.demo.repository.ClientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ClientService implements IClientService {

    @Autowired
    private ClientMapper clientMapper;

    @Override
    public Client getByClientId(int ClientId) {
        return clientMapper.getClientById(ClientId);
    }

    @Override
    public List<Client> getAllClients() {
        return clientMapper.getAllClients();
    }

    @Override
    public void addClient(Client Client) {
        clientMapper.insert(Client);
    }

    @Override
    public boolean updateClient(Client Client) {
        Client before = getByClientId(Client.getId());
        if(before == null){
            return false;
        }
        clientMapper.updateClient(Client);
        if(!before.getName().equals(Client.getName()) || !before.getCiudad().equals(Client.getCiudad()) || before.getDireccion() != Client.getDireccion() || before.getFax().equals(Client.getFax())|| before.getPais().equals(Client.getPais()) || before.getTitulo().equals(Client.getTitulo())){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean updateClientName(String name, int id) {
        Client before = getByClientId(id);
        if(before == null){
            return false;
        }

        clientMapper.updateClientName(name, id);
        if(!before.getName().equals(name)){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean deleteClient(int ClientId) {
        clientMapper.deleteClient(ClientId);
        if(getByClientId(ClientId) == null){
            return true;
        }else {
            return false;
        }
    }
}
