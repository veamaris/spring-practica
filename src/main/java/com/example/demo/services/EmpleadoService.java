package com.example.demo.services;

import com.example.demo.entities.Empleado;
import com.example.demo.repository.EmpleadoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmpleadoService implements IEmpleadoService{
    @Autowired
    private EmpleadoMapper empleadoMapper;

    @Override
    public void insert(Empleado empleado) {
        empleadoMapper.insertEmpleados(empleado);
    }

    @Override
    public void delete(int id) {
        empleadoMapper.deleteEmpleados(id);
    }

    @Override
    public boolean update(Empleado empleado) {
        empleadoMapper.updateEmpleados(empleado);
        return true;
    }

    @Override
    public Empleado getById(int id) {
        return empleadoMapper.getEmpleadosById(id);
    }

    @Override
    public List<Empleado> getAll() {
        return empleadoMapper.getAllEmpleados();
    }
}
