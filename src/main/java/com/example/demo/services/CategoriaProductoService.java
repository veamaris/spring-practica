package com.example.demo.services;

import com.example.demo.entities.CategoriaProducto;
import com.example.demo.repository.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoriaProductoService implements ICategoriaProducto{

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public void insert(CategoriaProducto categoriaProducto) {
        categoryMapper.insert(categoriaProducto);
    }

    @Override
    public void delete(int id) {
        categoryMapper.deleteCategoria(id);
    }

    @Override
    public boolean update(CategoriaProducto categoriaProducto) {
        categoryMapper.updateCategoria(categoriaProducto);
        return true;
    }

    @Override
    public CategoriaProducto getById(int id) {
        return categoryMapper.getCategoriaById(id);
    }

    @Override
    public List<CategoriaProducto> getAll() {
        return categoryMapper.getAllCategorias();
    }
}
