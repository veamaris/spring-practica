package com.example.demo.services;

import com.example.demo.entities.Proovedor;

import java.util.List;

public interface IProovedores {
    public void insert(Proovedor proovedor);
    public void delete(int id);
    public boolean update(Proovedor proovedor);
    public Proovedor getById(int id);
    public List<Proovedor> getAll();
}
