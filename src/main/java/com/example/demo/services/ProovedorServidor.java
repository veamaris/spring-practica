package com.example.demo.services;

import com.example.demo.entities.Proovedor;
import com.example.demo.repository.ProovedorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProovedorServidor implements IProovedores {

    @Autowired
    private ProovedorMapper proovedorMapper;

    @Override
    public void insert(Proovedor proovedor) {
        proovedorMapper.insertProovedor(proovedor);
    }

    @Override
    public void delete(int id) {
        proovedorMapper.deleteProovedor(id);
    }

    @Override
    public boolean update(Proovedor proovedor) {
        proovedorMapper.updateProovedor(proovedor);
        return true;
    }

    @Override
    public Proovedor getById(int id) {
        return proovedorMapper.getProovedoresId(id);
    }

    @Override
    public List<Proovedor> getAll() {
        return proovedorMapper.getAllProovedores();
    }
}
