package com.example.demo.services;

import java.util.List;

public interface ICategoryService {

    CategoriaProductoService getByCategoryId(final int id);
    List<CategoriaProductoService> getAllCategories();
    void addCategory(final CategoriaProductoService categoriaProducto);
    void updateCategory(final CategoriaProductoService categoriaProducto);
    void deleteCategory(final int id);
}
