package com.example.demo.services;

import com.example.demo.entities.Address;

import java.util.List;

public interface IAddressService {

    public void insert(Address address);
    public void delete(int id);
    public boolean update(Address address);
    public Address getById(int id);
    public List<Address> getAll();
}
