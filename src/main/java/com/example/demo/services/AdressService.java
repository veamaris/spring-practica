package com.example.demo.services;

import com.example.demo.entities.Address;
import com.example.demo.repository.AddressMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AdressService implements IAddressService {

    @Autowired
    private AddressMapper addressMapper;


    @Override
    public void insert(Address address) {
        addressMapper.insertAddress(address);
    }

    @Override
    public void delete(int id) {
        addressMapper.deleteAddress(id);
    }

    @Override
    public boolean update(Address address) {
        Address before = getById(address.getId());
        addressMapper.updateAddress(address);

        if(!address.getStreet().equals(before.getStreet()) || address.getContact() != before.getContact()){
            return true;
        }else{
            return  false;
        }
    }

    @Override
    public Address getById(int id) {
        return addressMapper.getAddressById(id);
    }

    @Override
    public List<Address> getAll() {
        return addressMapper.getAllAddresses();
    }
}
