package com.example.demo.services;

import com.example.demo.entities.CategoriaProducto;

import java.util.List;

public interface ICategoriaProducto {

    public void insert(CategoriaProducto categoriaProducto);
    public void delete(int id);
    public boolean update(CategoriaProducto categoriaProducto);
    public CategoriaProducto getById(int id);
    public List<CategoriaProducto> getAll();
}
