package com.example.demo.services;

import com.example.demo.entities.Contact;

import java.util.List;

public interface IContactService {

    Contact getByContactId(final int contactId);
    List<Contact> getAllContacts();
    void addContact(final Contact contact);
    boolean updateContact(final Contact contact);
    boolean updateContactName(String name, int id);
    boolean deleteContact(final int contactId);
}
