package com.example.demo.services;

import com.example.demo.entities.Client;

import java.util.List;

public interface IClientService {
    Client getByClientId(final int ClientId);
    List<Client> getAllClients();
    void addClient(final Client Client);
    boolean updateClient(final Client Client);
    boolean updateClientName(String name, int id);
    boolean deleteClient(final int ClientId);
}
