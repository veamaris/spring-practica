package com.example.demo.services;

import com.example.demo.entities.Empleado;

import java.util.List;

public interface IEmpleadoService {
    public void insert(Empleado empleado);
    public void delete(int id);
    public boolean update(Empleado empleado);
    public Empleado getById(int id);
    public List<Empleado> getAll();
}
