package com.example.demo.services;

import com.example.demo.entities.Order;

import java.util.List;

public interface IOrderService {
    List<Order> getOrdersById(final int id);
    List<Order> getAllOrders();
    void addOrder(final Order order);
    void updateOrder(final Order order);
    void deleteOrder(int id);

}
