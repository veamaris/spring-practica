package com.example.demo.services;

import com.example.demo.entities.Address;
import com.example.demo.entities.Contact;
import com.example.demo.repository.ContactRepository;
import org.apache.ibatis.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("contactService")
public class ContactService implements IContactService {

    @Autowired
   private ContactRepository contactRepository;

    @Override
    public Contact getByContactId(int contactId) {
        return contactRepository.getContactById(contactId);
    }

    @Override
    public List<Contact> getAllContacts() {
        return contactRepository.getAllContacts();
    }

    @Override
    public void addContact(Contact contact) {
        contactRepository.insert(contact);
    }

    @Override
    public boolean updateContact(Contact contact) {
        Contact before = getByContactId(contact.getId());
        if(before == null){
            return false;
        }
        contactRepository.updateContact(contact);
        if(!before.getName().equals(contact.getName()) || !before.getEmail().equals(contact.getEmail()) || before.getPhone() != contact.getPhone()){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean updateContactName(String name, int id) {
        Contact before = getByContactId(id);
        if(before == null){
            return false;
        }

        contactRepository.updateContactName(name, id);
        if(!before.getName().equals(name)){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean deleteContact(int contactId) {
        contactRepository.deleteContact(contactId);
        if(getByContactId(contactId) == null){
            return true;
        }else {
            return false;
        }
    }

}
