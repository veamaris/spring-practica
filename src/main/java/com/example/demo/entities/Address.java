package com.example.demo.entities;

import java.io.Serializable;

public class Address implements Serializable {
    private int id;
    private int contact;
    private String street;
    private Contact contactClass;

    public Address(int id, int contact) {
        this.id = id;
        this.contact = contact;
    }

    public Address() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getContact() {
        return contact;
    }

    public void setContact(int contact) {
        this.contact = contact;
    }

   public Contact getContactClass() {
        return contactClass;
    }

    public void setContactClass(Contact contactClass) {
        this.contactClass = contactClass;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
